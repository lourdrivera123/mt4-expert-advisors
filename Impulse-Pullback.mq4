//+------------------------------------------------------------------+
//|                                             Impulse Pullback.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#import "shell32.dll"
int ShellExecuteW(int hwnd,string Operation,string File,string Parameters,string Directory,int ShowCmd);
#import

datetime candleTime = 0;
string pairs[] = {"EURUSD","AUDUSD","GBPUSD"};
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
  void onStart() {
   Print(pairs[0]);
      Alert((string)ArraySize(pairs)+" number of pair");
      for(int x=0;x<=ArraySize(pairs)-1; x++)
        {
         Alert(pairs[x]);
        }
  }
int checkPair(string pair, datetime tickTime)
{
    datetime currentTime = tickTime;
    double prev6ema = iMA(pair, PERIOD_M15, 6, 0, MODE_EMA, PRICE_CLOSE,3);
    double prev18ema = iMA(pair, PERIOD_M15, 18, 0, MODE_EMA, PRICE_CLOSE, 3);
    
    double curr6ema = iMA(pair, PERIOD_M15, 6, 0, MODE_EMA, PRICE_CLOSE,2);
    double curr18ema = iMA(pair, PERIOD_M15, 18, 0, MODE_EMA, PRICE_CLOSE, 2);
    
    double curr50ema = iMA(pair, PERIOD_M15, 50, 0, MODE_EMA, PRICE_CLOSE,1);
    double curr200sma = iMA(pair, PERIOD_M15, 200, 0, MODE_SMA, PRICE_CLOSE,1);
    
    double hourly6ema = iMA(pair, PERIOD_H1, 6, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y18ema = iMA(pair, PERIOD_H1, 18, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y50ema = iMA(pair, PERIOD_H1, 50, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hourly200sma = iMA(pair, PERIOD_H1, 200, 0, MODE_SMA, PRICE_CLOSE, 0);

    if (
      currentTime != candleTime
      && (prev6ema < prev18ema && curr6ema > curr18ema)
      && (curr6ema > curr18ema && curr18ema > curr50ema && curr50ema > curr200sma)
      && (hourly6ema > hour1y18ema && hour1y18ema > hour1y50ema && hour1y50ema > hourly200sma)
     ){
      return 1;
    } else if (
      currentTime != candleTime
      && (prev6ema > prev18ema && curr6ema < curr18ema)
      && (curr6ema < curr18ema && curr18ema < curr50ema && curr50ema < curr200sma)
      && (hourly6ema < hour1y18ema && hour1y18ema < hour1y50ema && hour1y50ema < hourly200sma)
    ){
      return 2;
    }
    
    return 0;
    
    // Code for EMA bounce.
    // It should be on an uptrend or downtrend in H1 candles
    // m15 candles must be uptrend or downtrend related to higher candles
    // Ask price should be on hitting the ema 50
    // The nearest high is higher than the previous 10 or 20 candles
    
    
    // Code for taking a snapshot of the chart.
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
  //---
  for(int x=0;x<=ArraySize(pairs)-1; x++)
  {
    string pair = pairs[x];
    if(checkPair(pair, Time[0]) == 1) {
      Alert(pair+" -- Impulse pullback candidate (LONG)");
      SendNotification(pair+" -- Impulse pullback candidate (LONG)");

      candleTime = Time[0];

      string filename = "chartscreenshot"+IntegerToString(candleTime)+".png";
      if(ChartScreenShot(0,filename,800,600,ALIGN_LEFT)) {
            int fh = FileOpen("file.csv", FILE_CSV|FILE_READ|FILE_WRITE,',');
            Print(fh);
            FileWrite(fh, pair,filename,"LONG",false);
            FileFlush(fh);
            FileClose(fh);
   
            ShellExecuteW(0,"open","c:\\Users\\Administrator\\AppData\\Roaming\\MetaQuotes\\Terminal\\3294B546D50FEEDA6BF3CFC7CF858DB7\\MQL4\\Files\\jarvisfxmessenger.bat","","",1);
      }
    } else if(checkPair(pair, Time[0]) == 2) {
      Alert(pair+" -- Impulse pullback candidate (SHORT)");
      SendNotification(pair+" -- Impulse pullback candidate (SHORT)");
      
      candleTime = Time[0];

      string filename = "chartscreenshot"+IntegerToString(candleTime)+".png";
      if(ChartScreenShot(0,filename,800,600,ALIGN_LEFT)) {
            int fh = FileOpen("file.csv", FILE_CSV|FILE_READ|FILE_WRITE,',');
            Print(fh);
            FileWrite(fh, pair,filename,"SHORT",false);
            FileFlush(fh);
            FileClose(fh);
   
            ShellExecuteW(0,"open","c:\\Users\\Administrator\\AppData\\Roaming\\MetaQuotes\\Terminal\\3294B546D50FEEDA6BF3CFC7CF858DB7\\MQL4\\Files\\jarvisfxmessenger.bat","","",1);
      }
    }
  }
  }
//+------------------------------------------------------------------+
