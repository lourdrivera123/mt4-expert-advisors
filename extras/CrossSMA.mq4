//+------------------------------------------------------------------+
//|                                                     CrossSMA.mq4 |
//|                                    Copyright � 2016, FxCraft.biz |
//|                                           http://www.fxcraft.biz |
//+------------------------------------------------------------------+
#property copyright "Copyright � 2016, FxCraft.biz"
#property link      "http://www.fxcraft.biz"
#property version   "1.00"
#property strict
input string I = "SMA settings";
input int MA_Period = 21;
input ENUM_MA_METHOD MA_Method =0;
input ENUM_APPLIED_PRICE MA_Price =0;

input string II = "Order settings";
input double lot =0.2;
input int stopLoss =100;
input int takeProfit=100;
input int magic=684;
input int slippage=10;
string msg="";
string msgNewBar="";
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
      msg = "";
      bool isb = isNewBar(Symbol(),Period());
      
      if(isb)
         {
            msgNewBar="";
            
            int signal = getOpenSignal();
            
            if(signal>0)
            {
               closeOrders(magic,OP_SELL);
               int ticketB = OrderSend(Symbol(),OP_BUY,lot, Ask,slippage,sltpValue(Ask-stopLoss*Point,stopLoss),sltpValue(Ask+takeProfit*Point,takeProfit),"",magic);
               LogZlecenie(ticketB,OP_BUY,lot,Ask,sltpValue(Ask-stopLoss*Point,stopLoss),sltpValue(Ask+takeProfit*Point,takeProfit),GetLastError());
	            drawArrow("Buy",1,20);
            }
            
            if(signal<0)
            {
               closeOrders(magic,OP_BUY);
               int ticketS = OrderSend(Symbol(),OP_SELL,lot, Bid,slippage,sltpValue(Bid+stopLoss*Point,stopLoss),sltpValue(Bid-takeProfit*Point,takeProfit),"",magic);
               LogZlecenie(ticketS,OP_SELL,lot,Bid,sltpValue(Bid+stopLoss*Point,stopLoss),sltpValue(Bid-takeProfit*Point,takeProfit),GetLastError());
	            drawArrow("Sell",-1,20);
            }
         }

         
  }
//+------------------------------------------------------------------+

int getOpenSignal()
{
   double sma_1 = iMA(Symbol(),Period(),MA_Period,0,MA_Method,MA_Price,1);
   
   if(norm(Close[1] - sma_1)<0)
   {
      for(int i =2;i<6;i++)
      {
         double sma_i = iMA(Symbol(),Period(),MA_Period,0,MA_Method,MA_Price,i);
   
         if(norm(Close[i] - sma_i)>0) return(-1);
         if(norm(Close[i] - sma_i)<0) break;
      }
   }
   if(norm(Close[1] - sma_1)>0)
   {
      for(int i =2;i<6;i++)
      {
         double sma_i = iMA(Symbol(),Period(),MA_Period,0,MA_Method,MA_Price,i);
   
         if(norm(Close[i] - sma_i)<0) return(1);
         if(norm(Close[i] - sma_i)>0) break;
      }
   }
   
   return(0);
   
}

double norm(double val)
{
   return(NormalizeDouble(val,Digits));
}


bool isNewBar(string symbol, int timeframe) {
 static int prevTime;
 bool newBar=false; 

 if(iTime(symbol,timeframe,0) !=prevTime) {
  newBar=true;
  prevTime=(int)iTime(symbol,timeframe,0) ;
 }

 return(newBar);
}

void LogZlecenie(int ticket, int typ, double loty, double cena, double sl, double tp, int err=0)
{
   Log("Wysyla zlecenie typu: " + EnumToString( (ENUM_ORDER_TYPE) typ)  +", loty = " + DoubleToStr(loty,2) + ", Cena Otwarcia = " + str(cena) + ", Stop Loss = " + str(sl) + ", Take Profit = " + str(tp));
   if(ticket>0) 
   {
      
      Log("Otwarto zlecenie " +  EnumToString( (ENUM_ORDER_TYPE) typ)  + ", ticket = " + IntegerToString(ticket));
      if(OrderSelect(ticket,SELECT_BY_POS))
      {
         Log("Lots = " + str(OrderLots()) + ", Cena otwarcia: " + str(OrderOpenPrice()) + ", Stop Loss = " + str(OrderStopLoss()) + ", Take Profit = " + str(OrderTakeProfit()));
      }
   }
   else
   {
      Log("Blad przy wysylaniu zlecenia " + EnumToString( (ENUM_ORDER_TYPE) typ)  + ", blad "  + IntegerToString(err));
   }
   
}

double sltpValue(double w1, int w2)
{
   if(w2 == 0)
   return (0);
   
   return (NormalizeDouble(w1, Digits));
}
void Log(string Msg)
{
  int f = FileOpen(WindowExpertName()+"_"+Symbol()+"_"+IntegerToString(Period())+".log",FILE_READ|FILE_WRITE);

  if(f>0)
  {
     FileSeek(f, 0, SEEK_END);
     FileWrite(f, TimeToStr(TimeCurrent(),TIME_DATE|TIME_SECONDS)," ", Msg);
     FileClose(f);
  }
}
string str(double value)
{
   return(DoubleToStr(value,Digits));
}

int countOrders(int oMagic,int oType) {

   int count=0;
 
   for(int i=0;i<OrdersTotal();i++) {
    if(OrderSelect(i,SELECT_BY_POS)) {
     if(OrderMagicNumber()==oMagic) {
      if(OrderSymbol()==Symbol()) {
       if(OrderType()==oType || oType<0) {
        count++;
        }
       }
      }
     }
    }
   
   
   return(count);
 
} 




void drawArrow(string iName,int signal,int margin,int code=0,color clr=Red,color clr2=Lime) {
 
 string name=iName+TimeToStr(Time[0],TIME_DATE|TIME_MINUTES|TIME_SECONDS);
 
 double p=0;

 if(signal>0) {
  //p=Low[0]-margin*Point;
  p=Low[iLowest(NULL,0,MODE_LOW,5)]-margin*Point;
 }

 if(signal<0) {
  //p=High[0]+margin*Point;
  p=High[iHighest(NULL,0,MODE_LOW,5)]+margin*Point;
 }
 
 if(signal!=0) {
 
  if(ObjectFind(name)==-1) {
   ObjectCreate(name,OBJ_ARROW,0,Time[0],p);
  }
  
  ObjectSet(name,OBJPROP_PRICE1,p);
  
 
  if(signal>0) {
  ObjectSet(name,OBJPROP_COLOR,clr2);
   if(code==0) {
    ObjectSet(name,OBJPROP_ARROWCODE,241);
   } else {
    ObjectSet(name,OBJPROP_ARROWCODE,code);
   }
  }
  
  if(signal<0) {
  ObjectSet(name,OBJPROP_COLOR,clr);
   if(code==0) {
    ObjectSet(name,OBJPROP_ARROWCODE,242);
   } else {
    ObjectSet(name,OBJPROP_ARROWCODE,code);
   }
  }
  
 } 
 

}


void closeOrders(int oMagic,int oType) 
{
   bool close;
   for(int i=0;i<OrdersTotal();i++) 
   {
      if(OrderSelect(i,SELECT_BY_POS)) 
      {
         if(OrderMagicNumber()==oMagic || oMagic<0) 
         {
            if(OrderSymbol()==Symbol()) 
            {
               if(OrderType()==oType || oType<0) 
               {
                  
                  if(OrderType()==OP_BUY) 
                  {
                     close = OrderClose(OrderTicket(),OrderLots(),NormalizeDouble(Bid,Digits),0);
                     i--;
                  }
                  if(OrderType()==OP_BUYSTOP) 
                  {
                     close = OrderDelete(OrderTicket());
                     i--;
                  }
                  if(OrderType()==OP_BUYLIMIT) 
                  {
                     close = OrderDelete(OrderTicket());
                     i--;
                  }
                  if(OrderType()==OP_SELL) 
                  { 
                     close = OrderClose(OrderTicket(),OrderLots(),NormalizeDouble(Ask,Digits),0);
                     i--;
                  }
                  if(OrderType()==OP_SELLSTOP) 
                  {
                     close = OrderDelete(OrderTicket());
                     i--;
                  }
                  if(OrderType()==OP_SELLLIMIT) 
                  {
                     close = OrderDelete(OrderTicket());
                     i--;
                  }
               }
            }
         }
      }
   }
}