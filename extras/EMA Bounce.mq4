//+------------------------------------------------------------------+
//|                                                   EMA Bounce.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

datetime candleTime = 0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
    datetime currentTime = Time[0];
    double prev6ema = iMA(NULL, 0, 6, 0, MODE_EMA, PRICE_CLOSE,3);
    double prev18ema = iMA(NULL, 0, 18, 0, MODE_EMA, PRICE_CLOSE, 3);
    
    double curr6ema = iMA(NULL, 0, 6, 0, MODE_EMA, PRICE_CLOSE,2);
    double curr18ema = iMA(NULL, 0, 18, 0, MODE_EMA, PRICE_CLOSE, 2);
    
   double curr50BounceEma = iMA(NULL, 0, 50, 0, MODE_EMA, PRICE_CLOSE,0);
    double curr50ema = iMA(NULL, 0, 50, 0, MODE_EMA, PRICE_CLOSE,1);
    double curr200sma = iMA(NULL, 0, 200, 0, MODE_SMA, PRICE_CLOSE,1);
    double hourly6ema = iMA(NULL, PERIOD_H1, 6, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y18ema = iMA(NULL, PERIOD_H1, 18, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y50ema = iMA(NULL, PERIOD_H1, 50, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hourly200sma = iMA(NULL, PERIOD_H1, 200, 0, MODE_SMA, PRICE_CLOSE, 0);
    
    if (
      currentTime != candleTime
      && (Ask <= curr50BounceEma)
      && (curr18ema > curr50ema && curr50ema > curr200sma)
      && (hour1y18ema > hour1y50ema && hour1y50ema > hourly200sma)
     ){
      Alert((string)Ask+" -- Ask price");
      //SendNotification((string)Symbol()+" -- Impulse pullback candidate (LONG)");
      
      if(ChartScreenShot(0,"chartscreenshot"+IntegerToString(candleTime)+".png",800,600,ALIGN_LEFT)) {
         Print("We've saved the screenshot ");
      }
      candleTime = Time[0];
        
    } 
    /*else if (
      currentTime != candleTime
      && (prev6ema > prev18ema && curr6ema < curr18ema)
      && (curr6ema < curr18ema && curr18ema < curr50ema && curr50ema < curr200sma)
      && (hourly6ema < hour1y18ema && hour1y18ema < hour1y50ema && hour1y50ema < hourly200sma)
    ){
      Alert((string)Symbol()+" -- Impulse pullback candidate (SHORT)");
      SendNotification((string)Symbol()+" -- Impulse pullback candidate (SHORT)");
      
      if(ChartScreenShot(0,"chartscreenshot"+IntegerToString(candleTime)+".png",800,600,ALIGN_LEFT)) {
               Print("We've saved the screenshot ");
         }
      candleTime = Time[0];
    }*/
   
  }
//+------------------------------------------------------------------+
