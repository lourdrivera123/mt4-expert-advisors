#property copyright	"Ahra - Forex Factory"
#property link			"http://www.forexfactory.com/arha"

#include "WinUser32.mqh"

#define UP			1
#define DOWN	-1

extern double	TakeProfit = 10;
extern double	StopLoss = 40;
extern bool		DynamicLots = true;
extern double	DynamicLossPercentage = 5; // Maximum account loss percentage if stoploss is hit
extern double	FixedLotSize = 1;
extern double	ScalingValue = 4;
extern int			MagicNumber = 05000;

int TradeDirection;
double PTP, RealPTP, ChartScale;
string ManualEntry = "Manual entry";
string Status = "Status";

int init()
{
	RealPTP = Point * 10;
	setChartScale();
	getLotSize();
	
	string directionText;
	if (Bid > iMA(NULL, 0, 10, 0, MODE_EMA, PRICE_CLOSE,  0))
	{
		TradeDirection = UP;
		directionText = "BUY";
	}
	else if (Bid < iMA(NULL, 0, 10, 0, MODE_EMA, PRICE_CLOSE,  0))
	{
		TradeDirection = DOWN;
		directionText = "SELL";
	}
	
	// Status
	ObjectCreate(Status, OBJ_LABEL, 0, 0, 0);
	ObjectSet(Status, OBJPROP_XDISTANCE, 10);
	ObjectSet(Status, OBJPROP_YDISTANCE, 20);
	ObjectSet(Status, OBJPROP_BACK, false);
	string sl = DoubleToStr(StopLoss * ChartScale, 0);
	string tp = DoubleToStr(TakeProfit * ChartScale, 0);
	ObjectSetText(Status, "Waiting for " + directionText + " retrace, TP " + tp + ", SL " + sl, 12, "Arial Bold", Yellow);

	// Manual entry
	ObjectCreate(ManualEntry, OBJ_LABEL, 0, 0, 0);
	ObjectSet(ManualEntry, OBJPROP_CORNER, 2);
	ObjectSet(ManualEntry, OBJPROP_XDISTANCE, 10);
	ObjectSet(ManualEntry, OBJPROP_YDISTANCE, 20);
	ObjectSetText(ManualEntry, "Delete this to enter immediately", 8, "Arial", White);

	return(0);
}

int deinit()
{
	ObjectDelete(ManualEntry);
	ObjectDelete(Status);
	
	return(0);
}

int start()
{
	bool result;
	
	bool enterNow;
	if (ObjectFind(ManualEntry) == -1)
		enterNow = true;
		
	if (TradeDirection == UP && (enterNow || Bid < iMA(NULL, 0, 10, 0, MODE_EMA, PRICE_CLOSE,  0)))
	{
		result = enter(UP);
	}
	else if (TradeDirection == DOWN && (enterNow || Bid > iMA(NULL, 0, 10, 0, MODE_EMA, PRICE_CLOSE,  0)))
	{
		result = enter(DOWN);
	}
	
	if (result)
	{
		// Trade is made, so remove the bot
		PostMessageA( WindowHandle( Symbol(), Period()), WM_COMMAND, 33050, 0);
	}
	
	return(0);
}

double getLotSize()
{
	double lots;
	if (DynamicLots)
	{
		double maxLoss = AccountBalance() * (DynamicLossPercentage / 100);
		double stoplossPerOneLot = MarketInfo(Symbol(), MODE_TICKVALUE) * ((StopLoss * PTP) / MarketInfo(Symbol(), MODE_TICKSIZE));
		double maxLotsPossible = AccountBalance() / MarketInfo(Symbol(), MODE_MARGINREQUIRED) * 0.9; // 90% of absolute max
		lots = maxLoss / stoplossPerOneLot;
		if (lots > maxLotsPossible)
			lots = maxLotsPossible;
	}
	else
	{
		lots = FixedLotSize;
	}
	
	return(lots);
}

bool enter(int direction)
{
	int ticket;
	double lots;

	if (direction == UP)
	{
		lots = getLotSize();

		ticket = OrderSend(Symbol(), OP_BUY, lots, Ask, 3, 0, 0, "Buy order", MagicNumber, 0, Blue);
		Print("Buy trade with ticket #" + ticket);
		if (ticket != -1)
		{
			OrderSelect(ticket, SELECT_BY_TICKET);
			OrderModify(ticket, OrderOpenPrice(), OrderOpenPrice() - StopLoss * PTP, OrderOpenPrice() + TakeProfit * PTP, 0, CLR_NONE);
		}
	}
	else if (direction == DOWN)
	{
		lots = getLotSize();
		ticket = OrderSend(Symbol(), OP_SELL, lots, Bid, 3, 0, 0, "Sell order", MagicNumber, 0, Blue);
		Print("Sell trade with ticket #" + ticket);
		if (ticket != -1)
		{
			OrderSelect(ticket, SELECT_BY_TICKET);
			OrderModify(ticket, OrderOpenPrice(), OrderOpenPrice() + StopLoss * PTP, OrderOpenPrice() - TakeProfit * PTP, 0, CLR_NONE);
		}
	}
	
	if (ticket != -1)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

void setChartScale()
{
	int bars = 1000;
	int i, count;
	double sum;
	for (i = 1; i <= bars; i++)
	{
		sum += MathAbs(Open[i] - Close[i]) / RealPTP;
	}
	
	double mean = sum / bars;
	
	ChartScale = mean / ScalingValue;
	PTP = RealPTP * ChartScale;
}
