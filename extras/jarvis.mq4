//+------------------------------------------------------------------+
//|                                             jarvis.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#import "shell32.dll"
int ShellExecuteW(int hwnd,string Operation,string File,string Parameters,string Directory,int ShowCmd);
#import

datetime candleTime = 0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
Alert("STUFF ON TICK");
    /**datetime currentTime = Time[0];
    double prev6ema = iMA(NULL, PERIOD_M15, 6, 0, MODE_EMA, PRICE_CLOSE,3);
    double prev18ema = iMA(NULL, PERIOD_M15, 18, 0, MODE_EMA, PRICE_CLOSE, 3);
    
    double curr6ema = iMA(NULL, PERIOD_M15, 6, 0, MODE_EMA, PRICE_CLOSE,2);
    double curr18ema = iMA(NULL, PERIOD_M15, 18, 0, MODE_EMA, PRICE_CLOSE, 2);
    
    double curr50ema = iMA(NULL, PERIOD_M15, 50, 0, MODE_EMA, PRICE_CLOSE,1);
    double curr200sma = iMA(NULL, PERIOD_M15, 200, 0, MODE_SMA, PRICE_CLOSE,1);
    
    double hourly6ema = iMA(NULL, PERIOD_H1, 6, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y18ema = iMA(NULL, PERIOD_H1, 18, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hour1y50ema = iMA(NULL, PERIOD_H1, 50, 0, MODE_EMA, PRICE_CLOSE, 0);
    double hourly200sma = iMA(NULL, PERIOD_H1, 200, 0, MODE_SMA, PRICE_CLOSE, 0);
    
    double thirdCandleHigh = iHigh(NULL,PERIOD_M15,3);
    double secondCandleHigh = iHigh(NULL,PERIOD_M15,2);
    double previousCandleHigh = iHigh(NULL,PERIOD_M15,1);
    
    double thirdCandleLow = iHigh(NULL,PERIOD_M15,3);
    double secondCandleLow = iHigh(NULL,PERIOD_M15,2);
    double previousCandleLow = iHigh(NULL,PERIOD_M15,1);

    if (
      currentTime != candleTime
      && (prev6ema < prev18ema && curr6ema > curr18ema)
      && (curr6ema > curr18ema && curr18ema > curr50ema && curr50ema > curr200sma)
      && (hourly6ema > hour1y18ema && hour1y18ema > hour1y50ema && hour1y50ema > hourly200sma)
      && (previousCandleHigh < secondCandleHigh || secondCandleHigh < thirdCandleHigh)
     ){
      string pullbackHigh = (previousCandleHigh < secondCandleHigh) ? (string)previousCandleHigh: (string)secondCandleHigh;
      Alert((string)Symbol()+" -- Impulse pullback candidate (LONG) -- "+pullbackHigh);
      SendNotification((string)Symbol()+" -- Impulse pullback candidate (LONG) -- "+pullbackHigh);

      candleTime = Time[0];

      string filename = "chartscreenshot"+IntegerToString(candleTime)+".png";
      if(ChartScreenShot(0,filename,800,600,ALIGN_LEFT)) {
            int fh = FileOpen("file.csv", FILE_CSV|FILE_READ|FILE_WRITE,',');
            string msg = "LONG-"+pullbackHigh;
            Print(fh);
            FileWrite(fh, (string)Symbol(),filename,msg,false);
            FileFlush(fh);
            FileClose(fh);
   
            ShellExecuteW(0,"open","c:\\Users\\Zem\\AppData\\Roaming\\MetaQuotes\\Terminal\\3294B546D50FEEDA6BF3CFC7CF858DB7\\tester\\files\\jarvisfxmessenger.bat","","",1);
      }
    } else if (
      currentTime != candleTime
      && (prev6ema > prev18ema && curr6ema < curr18ema)
      && (curr6ema < curr18ema && curr18ema < curr50ema && curr50ema < curr200sma)
      && (hourly6ema < hour1y18ema && hour1y18ema < hour1y50ema && hour1y50ema < hourly200sma)
      && (previousCandleLow > secondCandleLow || secondCandleLow > thirdCandleLow)
    ){
      string pullbackLow = (previousCandleLow > secondCandleLow) ? (string)previousCandleLow: (string)secondCandleLow;
      Alert((string)Symbol()+" -- Impulse pullback candidate (SHORT) -- "+pullbackLow);
      SendNotification((string)Symbol()+" -- Impulse pullback candidate (SHORT) -- "+pullbackLow);
      
      candleTime = Time[0];

      string filename = "chartscreenshot"+IntegerToString(candleTime)+".png";
      if(ChartScreenShot(0,filename,800,600,ALIGN_LEFT)) {
            int fh = FileOpen("file.csv", FILE_CSV|FILE_READ|FILE_WRITE,',');
            string msg = "SHORT-"+pullbackLow;
            Print(fh);
            FileWrite(fh, (string)Symbol(),filename,msg,false);
            FileFlush(fh);
            FileClose(fh);
   
            ShellExecuteW(0,"open","c:\\Users\\Zem\\AppData\\Roaming\\MetaQuotes\\Terminal\\3294B546D50FEEDA6BF3CFC7CF858DB7\\tester\\files\\jarvisfxmessenger.bat","","",1);
      }
    }**/
  }
//+------------------------------------------------------------------+
